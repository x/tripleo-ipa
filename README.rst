===========
tripleo-ipa
===========

This repository contains Ansible for use integrating TripleO with FreeIPA.

Note that the master branch is deprecated, we still support the last release
in the stable/wallaby branch.
